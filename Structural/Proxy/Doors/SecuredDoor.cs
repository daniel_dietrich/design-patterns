using static System.Console;

namespace Proxy.Doors
{
    class SecuredDoor : IDoor
    {
        private static string Password = "P4$$W0RD";
        private IDoor door;

        public SecuredDoor(IDoor door)
        {
            this.door = door;
        }

        public void Open(string password)
        {
            if (password.Equals(Password))
            {
                door.Open();
            }
            else
            {
                WriteLine("Wrong password");
            }
        }

        public void Close()
        {
            door.Close();
        }
    }
}