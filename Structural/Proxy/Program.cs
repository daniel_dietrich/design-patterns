﻿using Proxy.Doors;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            var door = new SecuredDoor(new LabDoor());
            door.Open("invalid");

            door.Open("P4$$W0RD");
            door.Close();
        }
    }
}
