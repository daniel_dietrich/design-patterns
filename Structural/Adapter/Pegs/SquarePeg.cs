namespace Adapter.Pegs
{
    public class SquarePeg
    {
        public double Width { get; }

        public SquarePeg(double width)
        {
            Width = width;
        }
    }
}
