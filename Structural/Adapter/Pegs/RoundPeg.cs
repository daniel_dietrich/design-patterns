namespace Adapter.Pegs
{
    public class RoundPeg
    {
        public double Radius { get; }

        public RoundPeg(double radius)
        {
            Radius = radius;
        }
    }
}
