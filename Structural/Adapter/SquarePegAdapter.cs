using Adapter.Pegs;

namespace Adapter
{
    public class SquarePegAdapter : RoundPeg
    {
        public SquarePegAdapter(SquarePeg squarePeg) : base(WidthToRadius(squarePeg.Width)) { }

        private static double WidthToRadius(double width) => (width * System.Math.Sqrt(2) / 2);
    }
}
