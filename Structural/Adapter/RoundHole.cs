using Adapter.Pegs;

namespace Adapter
{
    public class RoundHole
    {
        public double Radius { get; }

        public RoundHole(double radius)
        {
            Radius = radius;
        }

        public bool Fits(RoundPeg roundPeg) => (Radius >= roundPeg.Radius);
    }
}
