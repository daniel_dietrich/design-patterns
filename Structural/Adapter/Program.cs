﻿using System;
using Adapter.Pegs;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            var roundHole = new RoundHole(10);

            var roundPeg = new RoundPeg(10);
            var squarePeg = new SquarePeg(11);
            var squarePegAdapter = new SquarePegAdapter(squarePeg);

            Console.WriteLine($"Round peg radius: {roundPeg.Radius}, peg fits the hole? {roundHole.Fits(roundPeg)}");
            Console.WriteLine($"Square peg adapter radius: {squarePegAdapter.Radius}, peg fits the hole? {roundHole.Fits(squarePegAdapter)}");
        }
    }
}
