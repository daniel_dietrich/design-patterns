﻿using Decorator.Data;
using Decorator.Data.Decorators;

using static System.Console;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainDataSource = new DataSource();
            var compressionDecorator = new CompressionDecorator(mainDataSource);
            var encryptionDecorator = new EncryptionDecorator(compressionDecorator);

            encryptionDecorator.WriteData("secret");

            WriteLine();
            WriteLine(encryptionDecorator.ReadData());
        }
    }
}
