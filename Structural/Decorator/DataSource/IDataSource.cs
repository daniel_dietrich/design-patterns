namespace Decorator.Data
{
    public interface IDataSource
    {
        void WriteData(string data);
        string ReadData();
    }
}
