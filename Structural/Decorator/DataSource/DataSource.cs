using static System.Console;

namespace Decorator.Data
{
    public class DataSource : IDataSource
    {
        private string data;
        private const string NoDataMsg = "NO DATA";

        public void WriteData(string data)
        {
            this.data = data;
            WriteLine($"Writing the {data}");
        }

        public string ReadData() => data ?? NoDataMsg;
    }
}
