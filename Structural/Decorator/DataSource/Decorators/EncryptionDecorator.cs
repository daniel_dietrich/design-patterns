using static System.Console;

namespace Decorator.Data.Decorators
{
    public class EncryptionDecorator : DataSourceDecorator
    {
        public EncryptionDecorator(IDataSource dataSource) : base(dataSource) { }

        public override void WriteData(string data)
        {
            WriteLine($"Encrypting the {data}");
            base.WriteData($"encrypted {data}");
        }
    }
}
