using static System.Console;

namespace Decorator.Data.Decorators
{
    public class CompressionDecorator : DataSourceDecorator
    {
        public CompressionDecorator(IDataSource dataSource) : base(dataSource) { }

        public override void WriteData(string data)
        {
            WriteLine($"Compressing the {data}");
            base.WriteData($"compressed {data}");
        }
    }
}
