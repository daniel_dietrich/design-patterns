namespace Decorator2.Coffee
{
    public interface ICoffee
    {
        public int Price();
        public string Description();
    }
}
