namespace Decorator2.Coffee.Decorator
{
    public class MilkCoffee : CoffeeDecorator
    {
        public MilkCoffee(ICoffee coffee) : base(coffee) { }

        public override int Price() => coffee.Price() + 1;

        public override string Description() => $"{coffee.Description()} & milk";
    }
}
