using System.Collections.Generic;
using System;

namespace Flyweight.Tea
{
    class TeaShop
    {
        private Dictionary<int, BaseTea> orders = new();
        private TeaMaker teaMaker;

        public TeaShop(TeaMaker teaMaker)
        {
            this.teaMaker = teaMaker;
        }

        public void TakeOrder(string teaType, int tableNumber)
        {
            orders[tableNumber] = teaMaker.Make(teaType);
        }

        public void Serve()
        {
            foreach (var order in orders)
            {
                Console.WriteLine($"Serving {order.Value.TeaType} tea to table #{order.Key}");
            }
        }
    }
}