namespace Flyweight.Tea
{
    class BaseTea
    {
        public string TeaType { get; }

        public BaseTea(string teaType)
        {
            TeaType = teaType;
        }
    }
}