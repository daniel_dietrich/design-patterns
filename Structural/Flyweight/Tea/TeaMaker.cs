using System.Collections.Generic;

namespace Flyweight.Tea
{
    class TeaMaker
    {
        private Dictionary<string, BaseTea> availableTea = new();

        public BaseTea Make(string teaType)
        {
            if (!availableTea.ContainsKey(teaType))
            {
                availableTea[teaType] = new(teaType);
            }
            return availableTea[teaType];
        }
    }
}