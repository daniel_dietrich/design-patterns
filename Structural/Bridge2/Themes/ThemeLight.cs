namespace Bridge2.Themes
{
    public class ThemeLight : ITheme
    {
        public string GetColor() => "Light";
    }
}
