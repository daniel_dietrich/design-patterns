namespace Bridge2.Themes
{
    public class ThemeDark : ITheme
    {
        public string GetColor() => "Dark";
    }
}
