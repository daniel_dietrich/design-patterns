using Bridge2.Themes;

namespace Bridge2.Pages
{
    public class PageAbout : WebPage
    {
        public PageAbout(ITheme theme) : base(theme, "About") { }
    }
}
