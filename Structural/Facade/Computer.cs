namespace Facade
{
    using static System.Console;

    public class Computer
    {
        public void Startup() => WriteLine("Starting up");

        public void PlaySound() => WriteLine("Playing sound");

        public void StartProcesses() => WriteLine("Starting system processes");

        public void EndProcesses() => WriteLine("Ending system processes");

        public void SaveProgress() => WriteLine("Saving progress");

        public void Shutdown() => WriteLine("Shooting down");
    }
}