namespace Facade
{
    public class ComputerFacade
    {
        protected Computer Computer { get; }

        public ComputerFacade(Computer computer)
        {
            Computer = computer;
        }

        public void TurnOn()
        {
            Computer.Startup();
            Computer.StartProcesses();
            Computer.PlaySound();
        }

        public void TurnOff()
        {
            Computer.SaveProgress();
            Computer.EndProcesses();
            Computer.Shutdown();
        }
    }
}