namespace Composite2.Employees
{
    interface IEmployee
    {
        public string Name { get; }
        public string Role { get; }
        public int Salary { get; }
    }
}
