namespace Composite2.Employees
{
    class Designer : IEmployee
    {
        public string Name { get; }
        public string Role { get; } = "Designer";
        public int Salary { get; }

        public Designer(string name, int salary)
        {
            Name = name;
            Salary = salary;
        }
    }
}
