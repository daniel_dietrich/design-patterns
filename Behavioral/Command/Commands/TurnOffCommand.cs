using Command;

namespace Command.Commands
{
    class TurnOffCommand : ICommand
    {
        private Television television;

        public TurnOffCommand(Television television)
        {
            this.television = television;
        }

        public void Execute() => television.TurnOff();

        public void Undo() => television.TurnOn();
    }
}