using static System.Console;

namespace Visitor.Animals
{
    class Lion : IAnimal
    {
        public string Name => "Lion";

        public void Scratch() => WriteLine($"{Name} is scratching.");

        public void Run() => WriteLine($"{Name} is running.");

        public void Accept(IAnimalOperation animalOperation)
        {
            animalOperation.VisitLion(this);
        }
    }
}