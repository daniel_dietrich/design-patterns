﻿using Visitor.Animals;
using Visitor.Operations;

using static System.Console;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            var chase = new ChaseOperation();
            var attack = new AttackOperation();

            var eagle = new Eagle();
            var lion = new Lion();
            var shark = new Shark();

            eagle.Accept(chase);
            lion.Accept(chase);
            shark.Accept(chase);
            WriteLine();

            eagle.Accept(attack);
            lion.Accept(attack);
            shark.Accept(attack);
        }
    }
}
