using Visitor.Animals;

namespace Visitor.Operations
{
    class AttackOperation : IAnimalOperation
    {
        public void VisitEagle(Eagle eagle) => eagle.Peck();

        public void VisitLion(Lion lion) => lion.Scratch();

        public void VisitShark(Shark shark) => shark.Bite();
    }
}