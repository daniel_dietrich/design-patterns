using Visitor.Animals;

namespace Visitor.Operations
{
    class ChaseOperation : IAnimalOperation
    {
        public void VisitShark(Shark dolphin) => dolphin.Swim();

        public void VisitEagle(Eagle eagle) => eagle.Fly();

        public void VisitLion(Lion lion) => lion.Run();
    }
}