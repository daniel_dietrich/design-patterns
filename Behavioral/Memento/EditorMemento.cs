namespace Memento
{
    record EditorMemento
    {
        public string Content { get; set; }
    }
}