﻿using static System.Console;

namespace Memento
{
    class Program
    {
        static void Main(string[] args)
        {
            var editor = new Editor();

            editor.Write("The first sentence.");
            editor.Write("The second sentence.");
            editor.Save();

            editor.Write("This is third sentence.");
            WriteLine(editor.Content);

            editor.Restore();
            WriteLine(editor.Content);
        }
    }
}
