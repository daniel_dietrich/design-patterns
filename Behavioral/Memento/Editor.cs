namespace Memento
{
    class Editor
    {
        public string Content { get; private set; }
        private EditorMemento memento;

        public Editor()
        {
            memento = new EditorMemento();
        }

        public void Write(string words)
        {
            Content += $" {words}";
        }

        public void Save()
        {
            memento.Content = Content;
        }

        public void Restore()
        {
            Content = memento.Content;
        }
    }
}