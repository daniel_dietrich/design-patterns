﻿using ChainOfResponsibility.Accounts;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            var bankAccount = new BankAccount(100);
            var paypalAccount = new PaypalAccount(200);
            var cryptoAccount = new CryptoAccount(300);

            bankAccount.SetNextAccount(paypalAccount);
            paypalAccount.SetNextAccount(cryptoAccount);

            bankAccount.Pay(299);
        }
    }
}
