namespace ChainOfResponsibility.Accounts
{
    class BankAccount : Account
    {
        public BankAccount(decimal balance) : base(balance) { }
    }
}