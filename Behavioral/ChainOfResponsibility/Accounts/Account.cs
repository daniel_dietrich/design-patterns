using static System.Console;

namespace ChainOfResponsibility.Accounts
{
    abstract class Account
    {
        private Account successorAccount;
        public decimal Balance { get; protected set; }

        public Account(decimal balance)
        {
            Balance = balance;
        }

        public void SetNextAccount(Account account)
        {
            successorAccount = account;
        }

        public void Pay(decimal amountToPay)
        {
            if (IsPaymentPossible(amountToPay))
            {
                WriteLine($"Paid {amountToPay:c} using {GetType().Name}.");
                Balance -= amountToPay;
            }
            else if (successorAccount is not null)
            {
                WriteLine($"Cannot pay using {GetType().Name}.");
                successorAccount.Pay(amountToPay);
            }
            else
            {
                WriteLine("None of the accounts have enough balance");
            }
        }

        private bool IsPaymentPossible(decimal amount) => (Balance >= amount);
    }
}
