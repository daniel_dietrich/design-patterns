namespace ChainOfResponsibility.Accounts
{
    class PaypalAccount : Account
    {
        public PaypalAccount(decimal balance) : base(balance) { }
    }
}