using System.Collections.Generic;
using System;

using static System.Console;

namespace Observer2
{
    public class Subject : ISubject
    {
        public int State { get; set; }

        private List<IObserver> observers = new();

        public void Attach(IObserver observer)
        {
            WriteLine("Subject: Attached an observer.");
            observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            WriteLine("Subject: Detached an observer.");
            observers.Remove(observer);
        }

        public void Notify()
        {
            WriteLine("Subject: Notifying observers...");
            observers.ForEach(o => o.Update(this));
        }

        public void ChangeState()
        {
            State = new Random().Next(0, 10);
            WriteLine("Subject: My state has just changed to: " + State);
            Notify();
        }
    }
}