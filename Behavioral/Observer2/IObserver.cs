namespace Observer2
{
    public interface IObserver
    {
        public void Update(ISubject subject);
    }
}