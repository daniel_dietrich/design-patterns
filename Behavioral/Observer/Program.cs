﻿namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {

            //Create publisher
            var jobPostings = new JobPostings();

            //Create Subscribers
            var johnDoe = new JobSeeker("John Doe");
            var janeDoe = new JobSeeker("Jane Doe");

            //Attach subscribers
            jobPostings.Subscribe(johnDoe);
            jobPostings.Subscribe(janeDoe);

            //Add a new job and see if subscribers get notified
            jobPostings.AddJobPost(new JobPost("Software Engineer"));
        }
    }
}
