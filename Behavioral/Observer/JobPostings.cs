using System;
using System.Collections.Generic;

namespace Observer
{
    class JobPostings : IObservable<JobPost>
    {
        private List<IObserver<JobPost>> observers = new();
        private List<JobPost> jobPostings = new();

        public IDisposable Subscribe(IObserver<JobPost> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }

            return new Unsubscriber<JobPost>(observers, observer);
        }

        public void AddJobPost(JobPost jobPost)
        {
            jobPostings.Add(jobPost);
            Notify(jobPost);
        }

        private void Notify(JobPost jobPost)
        {
            observers.ForEach(observer => observer.OnNext(jobPost));
        }
    }
}