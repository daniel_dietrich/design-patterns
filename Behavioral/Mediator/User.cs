namespace Mediator
{
    class User
    {
        public string Name { get; }
        private IChatroomMediator Chatroom { get; }

        public User(string name, IChatroomMediator chatroom)
        {
            Name = name;
            Chatroom = chatroom;
        }

        public void SendMessage(string message)
        {
            Chatroom.ShowMessage(this, message);
        }
    }
}