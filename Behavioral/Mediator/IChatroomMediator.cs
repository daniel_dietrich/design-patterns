namespace Mediator
{
    interface IChatroomMediator
    {
        void ShowMessage(User user, string message);
    }
}