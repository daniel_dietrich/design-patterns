namespace FactoryMethod.Trees
{
    public class SpruceTree : Tree
    {
        protected override int MinHeight => 12;
        protected override int MaxHeight => 16;

        public SpruceTree() : base("Spruce") { }
    }
}
