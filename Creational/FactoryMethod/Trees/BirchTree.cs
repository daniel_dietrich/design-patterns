namespace FactoryMethod.Trees
{
    public class BirchTree : Tree
    {
        protected override int MinHeight => 4;
        protected override int MaxHeight => 8;

        public BirchTree() : base("Birch") { }
    }
}
