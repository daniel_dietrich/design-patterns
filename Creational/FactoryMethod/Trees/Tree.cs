using System;

namespace FactoryMethod.Trees
{
    public abstract class Tree
    {
        private readonly Random random = new();

        private string Name { get; }
        private int Height { get; }

        protected abstract int MinHeight { get; }
        protected abstract int MaxHeight { get; }

        protected Tree(string name)
        {
            Name = name;
            Height = random.Next(MinHeight, MaxHeight);
        }

        public override string ToString() => $"{Name} is {Height} meters tall.";
    }
}
