﻿using System;
using Prototype.Shapes;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var circle = new Circle(0, 0, "green", 16);
            var circleCopy = circle.Clone();

            Console.WriteLine(circle.Area);
            Console.WriteLine(circleCopy.Area);
        }
    }
}
