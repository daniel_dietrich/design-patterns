namespace Prototype.Shapes
{
    public class Circle : Shape
    {
        private double Radius { get; }

        public override double Area => (System.Math.PI * (Radius * Radius));

        public Circle(int x, int y, string color, double radius) : base(x, y, color)
        {
            Radius = radius;
        }

        private Circle(Circle circle) : base(circle)
        {
            Radius = circle.Radius;
        }

        public override Circle Clone() => new(this);
    }
}
