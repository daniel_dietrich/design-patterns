namespace Prototype.Shapes
{
    public abstract class Shape
    {
        protected int X { get; }
        protected int Y { get; }
        protected string Color { get; }

        public abstract double Area { get; }
        protected Shape(int x, int y, string color)
        {
            X = x;
            Y = y;
            Color = color;
        }

        protected Shape(Shape shape)
        {
            X = shape.X;
            Y = shape.Y;
            Color = shape.Color;
        }
        public abstract Shape Clone();
    }
}
