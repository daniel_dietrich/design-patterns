using AbstractFactory.GUIElements;

namespace AbstractFactory.GUIFactories
{
    public abstract class GUIFactory
    {
        public abstract IButton GetButton();
        public abstract ICheckBox GetCheckBox();
    }
}
