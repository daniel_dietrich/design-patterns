using AbstractFactory.GUIElements.Win;

namespace AbstractFactory.GUIFactories.Win
{
    public class WinGUIFactory : GUIFactory
    {
        public override WinButton GetButton() => new WinButton();
        public override WinCheckBox GetCheckBox() => new WinCheckBox();
    }
}
