using AbstractFactory.GUIFactories;
using AbstractFactory.GUIFactories.Mac;
using AbstractFactory.GUIFactories.Win;

using System;

namespace AbstractFactory
{
    public class Application
    {
        public GUIFactory GuiFactory { get; }

        public Application(string os)
        {
            GuiFactory = os.ToLower() switch
            {
                "win" => new WinGUIFactory(),
                "mac" => new MacGUIFactory(),
                _ => throw new Exception($"[{os}] is not supported."),
            };
        }
    }
}
