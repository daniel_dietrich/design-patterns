namespace AbstractFactory.GUIElements
{
    public interface ICheckBox
    {
        void Paint();
    }
}
