using static System.Console;

namespace AbstractFactory.GUIElements.Mac
{
    public class MacCheckBox : ICheckBox
    {
        public void Paint() => WriteLine("Painting Mac CheckBox");
    }
}
