namespace AbstractFactory.GUIElements
{
    public interface IButton
    {
        void Paint();
    }
}
