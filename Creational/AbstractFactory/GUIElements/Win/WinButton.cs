using static System.Console;

namespace AbstractFactory.GUIElements.Win
{
    public class WinButton : IButton
    {
        public void Paint() => WriteLine("Painting Windows Button");
    }
}
