namespace FactoryMethod2.Interviewers
{
    public interface IInterviewer
    {
        void AskQuestions();
    }
}
