using FactoryMethod2.Interviewers;

namespace FactoryMethod2.HiringManagers
{
    public abstract class HiringManager
    {
        protected abstract IInterviewer CreateInterviewer();

        public void TakeInterview() => CreateInterviewer().AskQuestions();
    }
}
