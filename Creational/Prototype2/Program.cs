﻿using System;

namespace Prototype2
{
    class Program
    {
        static void Main(string[] args)
        {
            var original = new Sheep("Jolly", "Mountain sheep");
            var clone = original.Clone();

            Console.WriteLine(original.ToString());
            Console.WriteLine(clone.ToString());
        }
    }
}
