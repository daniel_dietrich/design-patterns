namespace Prototype2
{
    public class Sheep
    {
        private string Name { get; }
        private string Breed { get; }

        public Sheep(string name, string breed)
        {
            Name = name;
            Breed = breed;
        }

        public Sheep Clone() => MemberwiseClone() as Sheep;

        public override string ToString() => $"{Name} is a {Breed}.";
    }
}
