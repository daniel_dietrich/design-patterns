namespace Builder
{
    public record BurgerBuilder
    {
        public int Size { get; }
        public bool Cheese { get; private init; }
        public bool Pepperoni { get; private init; }
        public bool Lettuce { get; private init; }
        public bool Tomato { get; private init; }

        public BurgerBuilder(int size = 10) { Size = size; }

        public BurgerBuilder AddCheese() => this with { Cheese = true };

        public BurgerBuilder AddPepperoni() => this with { Pepperoni = true };

        public BurgerBuilder AddLettuce() => this with { Lettuce = true };

        public BurgerBuilder AddTomato() => this with { Tomato = true };

        public Burger Build() => new Burger(this);
    }
}
